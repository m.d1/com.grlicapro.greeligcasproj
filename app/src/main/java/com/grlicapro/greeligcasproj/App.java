package com.grlicapro.greeligcasproj;

import com.onesignal.OneSignal;
import com.squareup.okhttp.OkHttpClient;
import com.yandex.metrica.YandexMetrica;
import com.yandex.metrica.YandexMetricaConfig;

public class App extends android.app.Application {
    private OkHttpClient okHttpClient;

    @Override
    public void onCreate() {
        super.onCreate();

        // OneSignal Initialization
        OneSignal.initWithContext(this);
        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE);

        // YandexMetrica Initialization
        YandexMetricaConfig config=YandexMetricaConfig.newConfigBuilder("9b16c204-ebeb-4cf4-9e4d-b978abe8c44d").build();
        YandexMetrica.activate(getApplicationContext(),config);
        YandexMetrica.enableActivityAutoTracking(this);

        okHttpClient=new OkHttpClient();
    }
    public OkHttpClient getOkHttpClient(){return okHttpClient;}
}
