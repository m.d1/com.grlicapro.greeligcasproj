package com.grlicapro.greeligcasproj;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private AutoCompleteTextView autTV;
    private EditText editText;
    private Button verifyBtn;
    private App app;
    private FirebaseRemoteConfig firebaseRemoteConfig;
    private String source,crm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        verifyBtn.setOnClickListener(view -> {
            String phoneNumber=editText.getText().toString();
            if (phoneNumber.trim().isEmpty()){
                Toast.makeText(this, "Будь ласка, введіть номер телефону...", Toast.LENGTH_SHORT).show();
            }
            else
                if (phoneNumber.trim().length()!=9){
                    Toast.makeText(this, "Замало цифер...", Toast.LENGTH_SHORT).show();
                }
                else {
                verifyPhone(phoneNumber);
            }
        });
    }
    public void init(){
        autTV=findViewById(R.id.country_digits_f);
        editText=findViewById(R.id.phone_input_f);
        verifyBtn=findViewById(R.id.verify_button);
        firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(3600)
                .build();
        firebaseRemoteConfig.setDefaultsAsync(R.xml.remote_config_def);
        firebaseRemoteConfig.setConfigSettingsAsync(configSettings);
    }

    public void verifyPhone(String phone){
        firebaseRemoteConfig.fetchAndActivate()
                .addOnCompleteListener(this, task ->
                        source = firebaseRemoteConfig.getString("source"));
        crm = firebaseRemoteConfig.getString("crm");

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("source", source);
            jsonObject.put("country", "UA");
            jsonObject.put("phone", phone);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        OkHttpClient client=((App)getApplication()).getOkHttpClient();
        MediaType json=MediaType.parse("application/json; charset=utf-8");
        RequestBody requestBody=RequestBody.create(json,jsonObject.toString());
        Request request=new Request.Builder()
                .post(requestBody)
                .url(crm)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                runOnUiThread(() ->
                        Toast.makeText(MainActivity.this, "Невірний номер телефону !", Toast.LENGTH_LONG).show());
            }
            @Override
            public void onResponse(Response response) throws IOException {
                if (response.code() == 204) {
                    startActivity(new Intent(MainActivity.this,WebV.class));
                }
                else {
                    Toast.makeText(MainActivity.this, "re", Toast.LENGTH_SHORT).show();
                }

            }
        });
//        OkHttpClient client = //тут дістаеш okhttp (краще якщо він знаходиться в application)
//                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
//        RequestBody requestBody = RequestBody.create(JSON, jsonObject.toString());
//        Request request = new Request.Builder()
//                .post(requestBody)
//                .url("https://api.creditua.best/api/order?token=NUp81EWXBRARrCEeeOaCbILOmObouXmx" )  // crm - це урл той що для валідації номеру
//                .build();
//        client.newCall(request).enqueue(new Callback() {
//            @Override
//            public void onResponse(@NotNull Call call, @NotNull okhttp3.Response response) throws IOException {
//                if (response.code() == 204) {
//                    //show webview if code 204
//                }
//            }
//
//            @Override
//            public void onFailure(@NotNull Call call, @NotNull IOException e) {
//                //show wrong phone number error
//            }
//        });
    }
}