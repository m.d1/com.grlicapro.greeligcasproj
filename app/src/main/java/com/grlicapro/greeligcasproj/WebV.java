package com.grlicapro.greeligcasproj;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.facebook.FacebookSdk;
import com.facebook.applinks.AppLinkData;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import java.util.Objects;

public class WebV extends AppCompatActivity {

    private static final String TAG = "WebV";
    private boolean isLoadingSuccess=true;
    private WebView webView;
    private FirebaseRemoteConfig firebaseRemoteConfig;
    private String startUrl;
    private String startParams;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(3600)
                .build();
        firebaseRemoteConfig.setDefaultsAsync(R.xml.remote_config_def);
        firebaseRemoteConfig.setConfigSettingsAsync(configSettings);
        initDefaultUrlParamsFromFirebase();
        getFaceBookParams();
        generalInit();

    }

    public void generalInit() {
        webView = findViewById(R.id.web_v);
        CookieManager cookieManager = CookieManager.getInstance();
        CookieManager.setAcceptFileSchemeCookies(true);
        cookieManager.setAcceptThirdPartyCookies(webView, true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setDefaultTextEncodingName("utf-8");
        webView.setWebViewClient(new WebViewClient(){
            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                super.onReceivedHttpError(view, request, errorResponse);
                isLoadingSuccess = false;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                isLoadingSuccess = true;
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                Log.d(TAG, "onPageFinished: " + url);
                saveLastUrl(url);
            }
        });

        webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        webView.loadUrl(createUrlWithParams());

    }

    private void initDefaultUrlParamsFromFirebase() {
        firebaseRemoteConfig.fetchAndActivate()
                .addOnCompleteListener(this, task ->
                        startUrl = firebaseRemoteConfig.getString("start_url"));
                        startParams = firebaseRemoteConfig.getString("start_params");
    }

    private void getFaceBookParams() {
        FacebookSdk.setAutoInitEnabled(true);
        FacebookSdk.fullyInitialize();
        AppLinkData.fetchDeferredAppLinkData(getApplicationContext(), new AppLinkData.CompletionHandler() {
            @Override
            public void onDeferredAppLinkDataFetched(@Nullable AppLinkData appLinkData) {
                if (appLinkData != null && appLinkData.getTargetUri() != null
                        && appLinkData.getTargetUri().getAuthority() != null
                        && appLinkData.getTargetUri().getAuthority().contains("source")) {
                    startParams = appLinkData.getTargetUri().getAuthority();
                }
            }
        });
    }
    @Override
    public void onBackPressed() {
        if (webView != null && webView.canGoBack()) {
            webView.goBack();
        } else {
            super.onBackPressed();
        }
    }
    private void saveLastUrl(String url) {
        if (isLoadingSuccess) {
            SharedPreferences sharedPreferences = Objects.requireNonNull(getSharedPreferences("prefs", MODE_PRIVATE));
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("url", url);
            editor.apply();
        }
    }
    private String createUrlWithParams() {
        return getSharedPreferences("prefs", MODE_PRIVATE).getString("url", startUrl + startParams);

    }
}